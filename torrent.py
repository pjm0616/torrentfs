#!/usr/bin/python
# coding: utf-8 -*-

import sys
import math
import time
import sys
import os
import re

import gevent
import libtorrent as lt

## Exceptions
class SessionError(Exception):
	pass
class TorrentError(SessionError):
	def __init__(self, infohash, msg):
		SessionError.__init__('<%s>: %s' % (infohash, msg))
		self.infohash = infohash
		self.msg = msg
class InvalidTorrentError(TorrentError):
	pass

## Utilities
_re_magneturi = re.compile('^magnet:\\?xt=urn:btih:(?P<infohash>[0-9a-zA-Z]+)')
def parse_magneturi(uri):
	res = _re_magneturi.match(uri)
	if not res:
		raise SessionError('invalid magnet uri')
	return {'infohash': res.group('infohash')}

def add_suffix(val):
	prefix = ['B', 'kB', 'MB', 'GB', 'TB']
	for i in range(len(prefix)):
		if abs(val) < 1000:
			if i == 0:
				return '%5.3g%s' % (val, prefix[i])
			else:
				return '%4.3g%s' % (val, prefix[i])
		val /= 1000

	return '%6.3gPB' % val


class EventHandler(object):
	''' Event handler stub object '''
	def __init__(self):
		pass
	def state_changed(self, trnt, prevstate, state):
		pass
	def torrent_ready(self, trnt):
		pass
	def torrent_removed(self, infohash):
		pass
	def piece_finished(self, trnt, pieceidx):
		pass

class Torrent(object):
	''' Manages/handles/parses a torrent (a infohash) '''
	def __init__(self, sess, handle):
		self.sess = sess
		self.handle = handle

		self.infohash = str(handle.info_hash())
		# fixme: inconsistency: files[name] = {'size': size}, dirtree[name] = size
		self.files = {}
		self.dirtree = {}
		# WARNING: don't call self.handle.has_metadata
		self.has_metadata = False
		self._piece_priorities = []

		if handle.has_metadata():
			self.got_metadata()
		else:
			self.piece_length = 0
			self.npiece = 0

	def __parse_dir_structure(self):
		for entry, info in self.files.iteritems():
			splitted = entry.split('/')
			root = self.dirtree
			for name in splitted[:-1]:
				try:
					root = root[name]
				except KeyError:
					e = {}
					root[name] = e
					root = e
			name = splitted[-1]
			if name in root:
				raise InvalidTorrentError(self.infohash, 'invalid directory structure')
			root[name] = info['size']

	def got_metadata(self):
		if self.has_metadata:
			return
		print '*** Got metadata for %s' % self.infohash
		info = self.handle.get_torrent_info()
		self.piece_length = info.piece_length()
		self.npieces = info.num_pieces()
		self._piece_priorities = [0] * self.npieces
		self._parseinfo()

		if str(self.handle.status().state) != 'checking_files':
			# HACK: see _reset_piece_priorities, on_state_changed
			self._reset_piece_priorities()

		self.has_metadata = True
		self.sess.evhandler.torrent_ready(self)

	def _reset_piece_priorities(self):
		''' set every piece's priority to 0(=don't download) '''
		if str(self.handle.status().state) == 'checking_files':
			# HACK: libtorrent bug: assertion failure:
			# > function: void libtorrent::aux::session_impl::dequeue_check_torrent(const boost::shared_ptr<libtorrent::torrent>&)
			# > expression: t->state() == torrent_status::checking_files || t->state() == torrent_status::queued_for_checking
			# see also: on_state_changed()
			raise Exception('ljaouciwcier')
		self.handle.prioritize_pieces(self._piece_priorities)

	def _calculatepieces_byoffset(self, offset, size):
		''' calculate piece index containing torrent_data[offset:size] '''
		endoffset = offset + size

		# range: (piece_startoff @ `start' piece) ~ (piece_endoff @ `end' piece)
		start = offset // self.piece_length
		piece_startoff = offset % self.piece_length
		end = endoffset // self.piece_length
		piece_endoff = endoffset % self.piece_length

		if piece_endoff != 0 and end < self.npieces - 1:
			end += 1
		return start, end, piece_startoff, piece_endoff

	def _parseinfo(self):
		''' process torrent_info '''
		info = self.handle.get_torrent_info()
		files = info.files()
		for f in files:
			pieces = self._calculatepieces_byoffset(f.offset, f.size)
			self.files[f.path] = {
				'offset': f.offset,
				'size': f.size,
				'pieces': pieces,
			}
			#print '%s\t%r' % (f.path, self.files[f.path])
		self.__parse_dir_structure()

	def getpieces_byoffset(self, path, offset, size):
		''' returns range of pieces that contains `path'[offset:size] '''
		try:
			f = self.files[path]
		except KeyError:
			raise Exception('file not found')
		pieces = f['pieces']

		# relative piece index: (offset + [size] + offset in piece) / piece_length
		rel_start = (offset + pieces[2]) // self.piece_length
		rel_end = int(math.ceil(float(offset + size + pieces[2]) / self.piece_length))
		rel_end = min(rel_end, self.npieces-1)
		return (rel_start + pieces[0], rel_end + pieces[0])

	def request_data(self, path, offset, size):
		''' download path[offset:size] '''
		size += 1024 * 1024 * 20 # +20MiB: download more data for speed
		start, end = self.getpieces_byoffset(path, offset, size)
		#print 'request_data: downloading piece %d ~ %d' % (start, end)
		end = min(end, self.npieces - 1)
		for piece in xrange(start, end + 1):
			# libtorrent bug:
			# if we change priority of a downloaded piece, assertion
			# > function: void libtorrent::torrent::set_state(libtorrent::torrent_status::state_t)
			# > expression: std::accumulate(pieces.begin(), pieces.end(), 0) == 0
			# will fail
			if not self.handle.have_piece(piece):
				self._piece_priorities[piece] = 1
				self.handle.piece_priority(piece, 1)

	def isdownloaded(self, path, offset, size):
		if not self.has_metadata:
			return False
		start, end = self.getpieces_byoffset(path, offset, size)
		for piece in xrange(start, end+1):
			if not self.handle.have_piece(piece):
				return False
		return True

	def pause(self):
		self.handle.auto_managed(False)
		self.handle.pause()
	def resume(self):
		self.handle.resume()
		self.handle.auto_managed(True)

	def on_state_changed(self, prevstate, state):
		if prevstate == 'checking_files':
			# HACK(libtorrent bug): see trnt._reset_piece_priorities()
			# assertion will fail if we call handle.piece_priorities() while checking.
			# if we didn't call _reset_piece_priorities() in got_metadata(),
			# call _reset_piece_priorities() now.
			self._reset_piece_priorities()
		self.sess.evhandler.state_changed(self, prevstate, state)
	def on_piece_finished(self, pieceidx):
		self.sess.evhandler.piece_finished(self, pieceidx)
	def on_metadata_received(self):
		self.got_metadata()

class Session(object):
	'''
		libtorrent session.
	'''
	def __init__(self, evhandler):
		'''
			Initializes libtorrent session.
			Event notifications will be passed to `evhandler'.
			if you don't need event handling, pass EventHandler() as the argument.
			Downloaded files will be saved to './dn/' AND it will use ports 6881 ~ 6891.
		'''
		self.options = {
			'port_range': (6881, 6891),
			'default_ratio': 0.5,
			'max_upload_rate': -1,
			'max_download_rate': -1,
			'save_path': './dn/',
		}
		self.alive = False
		self.ltsess = None
		self.torrents = {}
		self.evhandler = evhandler

		self.__init_session()

		self.alert_receiver = None

	def __init_session(self):
		''' Initialize libtorrent session '''
		self.ltsess = lt.session()

		settings = lt.session_settings()
		settings.user_agent = 'lt-client/' + lt.version
		# we must not miss alerts as we relies on alerts.
		settings.alert_queue_size = 4096
		self.ltsess.set_settings(settings)
		self.ltsess.set_alert_mask(lt.alert.category_t.error_notification |
			lt.alert.category_t.status_notification |
			lt.alert.category_t.progress_notification |
			lt.alert.category_t.storage_notification)

		#self.ltsess.set_download_rate_limit(int(self.options['max_download_rate']))
		#self.ltsess.set_upload_rate_limit(int(self.options['max_upload_rate']))

		self.ltsess.add_extension(lt.create_ut_pex_plugin)
		self.ltsess.add_extension(lt.create_ut_metadata_plugin)
		self.ltsess.add_extension(lt.create_metadata_plugin)

		self.ltsess.add_dht_router('localhost', 56801) # DEBUG: for fast dht node discovery
		self.ltsess.add_dht_router('router.bittorrent.com', 6881)

	def start(self, autotick=True):
		'''
			Start the session. You have to call this before calling any other methods.
			If autotick == True, Session.tick() will be called automatically. You have to use gevent to use autoticks.
		'''
		self.alive = True
		self.ltsess.listen_on(*self.options['port_range'])
		self.ltsess.start_dht()
		if autotick:
			gevent.Greenlet.spawn(self._autotick_thread)
	def _autotick_thread(self):
		while self.alive:
			self.tick()
			gevent.sleep(0.5)

	def stop(self):
		''' Stop the session '''
		self.alive = False
		self.ltsess.stop_dht()
		self.ltsess.pause()

	def itertorrents(self):
		for t in self.torrents.itervalues():
			yield t

	def _add_torrent(self, ti=None, magnet=None):
		'''
			Add a torrent. If you have metadata, call with `ti'. If you have magnet URI, call with `magnet'.
			By default the torrent will stop when the metadata is downloaded. (all pieces' priorities will be set to 0)
		'''
		if ti:
			infohash = ti.info_hash()
		else:
			infohash = parse_magneturi(magnet)['infohash']
		infohash = str(infohash)
		if len(infohash) != 40:
			raise Exception('invalid infohash length')
		if infohash in self.torrents:
			return None

		atp = {}
		atp['save_path'] = self.options['save_path'] + infohash
		try:
			os.mkdir(atp['save_path'])
		except OSError:
			pass
		atp['storage_mode'] = lt.storage_mode_t.storage_mode_sparse
		atp['duplicate_is_error'] = True
		# we have to set paused=True to auto_manage the torrent.
		atp['paused'] = True
		# setting auto_managed=True automatically starts downloading
		atp['auto_managed'] = True

		if magnet:
			h = lt.add_magnet_uri(self.ltsess, magnet, atp)

			#libtorrent 0.15 does not support atp['url']
			#atp['url'] = magnet
			#h = self.ltsess.add_torrent(atp)
		else:
			atp['ti'] = ti
			h = self.ltsess.add_torrent(atp)
		if not h:
			return None
		trnt = Torrent(self, h)
		self.torrents[infohash] = trnt

		h.set_max_connections(60)
		h.set_max_uploads(-1)
		h.set_ratio(self.options['default_ratio'])

		return trnt

	def remove_torrent(self, trnt):
		infohash = trnt.infohash
		self.ltsess.remove_torrent(trnt.handle)
		try:
			del self.torrents[infohash]
			self.evhandler.torrent_removed(infohash)
		except KeyError:
			raise Exception('remove_torrent: torrent %s not found' % infohash)

	def add_file(self, path):
		e = lt.bdecode(open(path, 'rb').read())
		info = lt.torrent_info(e)
		return self._add_torrent(ti=info)
	def add_magnet(self, magnet):
		return self._add_torrent(magnet=magnet)
	def add_infohash(self, infohash):
		magnet = 'magnet:?xt=urn:btih:%s' % infohash
		return self._add_torrent(magnet=magnet)

	def tick(self):
		''' Updates the session. Call this every 500 msec. '''
		for i in xrange(1, 2000):
			if not self._tick_alert():
				break

	def _tick_alert(self):
		''' Handles newly received alerts. '''
		# we could alternatively use sess.wait_for_alert however writing multithreaded python code drives me crazy
		alert = self.ltsess.pop_alert()
		if not alert:
			return False
		if type(alert) in [lt.block_downloading_alert]:
			return
		print '\n  @@ALERT: %s\n'%str(alert)

		if type(alert) is lt.state_changed_alert:
			infohash = str(alert.handle.info_hash())
			try:
				trnt = self.torrents[infohash]
			except KeyError:
				raise Exception('tick_alert: torrent not found')
			prevstate, state = str(alert.prev_state), str(alert.state)
			trnt.on_state_changed(prevstate, state)
		elif type(alert) is lt.piece_finished_alert:
			infohash = str(alert.handle.info_hash())
			try:
				trnt = self.torrents[infohash]
			except KeyError:
				raise Exception('tick_alert: torrent not found')
			trnt.on_piece_finished(alert.piece_index)
		elif type(alert) is lt.metadata_received_alert:
			infohash = str(alert.handle.info_hash())
			try:
				trnt = self.torrents[infohash]
			except KeyError:
				raise Exception('tick_alert: torrent not found')
			trnt.on_metadata_received()
		return True


class TorrentServiceError(Exception):
	pass

class TorrentService(object):
	''' RPC interface for 'Session' '''
	def __init__(self, sess):
		self.sess = sess
	def _get_torrent(self, infohash):
		try:
			trnt = self.sess.torrents[infohash]
		except KeyError:
			raise TorrentServiceError('torrent %s not found' % infohash)
		return trnt

	def start(self):
		self.sess.start()
	def stop(self):
		self.sess.stop()
	def tick(self):
		self.sess.tick()

	##
	def add_file(self, path):
		trnt = self.sess.add_file(path)
		if not trnt:
			raise TorrentServiceError('add_file failed')
		return trnt.infohash
	def add_infohash(self, infohash):
		trnt = self.sess.add_infohash(infohash)
		if not trnt:
			raise TorrentServiceError('add_file failed')
		return trnt.infohash
	def remove_torrent(self, infohash):
		trnt = self._get_torrent(infohash)
		try:
			self.sess.remove_torrent(trnt)
		except:
			raise TorrentServiceError('failed to remove torrent %s' % infohash)
	def get_torrentlist(self):
		return self.sess.torrents.keys()

	##
	def get_filelist(self, infohash):
		trnt = self._get_torrent(infohash)
		if not trnt.has_metadata:
			return []
		return trnt.files.keys()
	def get_files(self, infohash):
		trnt = self._get_torrent(infohash)
		if not trnt.has_metadata:
			return []
		return trnt.files
	def get_dirtree(self, infohash):
		trnt = self._get_torrent(infohash)
		if not trnt.has_metadata:
			return []
		return trnt.dirtree
	def get_state(self, infohash):
		# 'checking_files', 'finished', etc.
		trnt = self._get_torrent(infohash)
		return trnt.handle.status().state
	def has_metadata(self, infohash):
		trnt = self._get_torrent(infohash)
		return trnt.has_metadata
	def get_torrentname(self, infohash):
		# not used
		trnt = self._get_torrent(infohash)
		if not trnt.has_metadata:
			return infohash
		return trnt.handle.get_torrent_info().name()

	##
	def get_filesize(self, infohash, path):
		trnt = self._get_torrent(infohash)
		try:
			f = trnt.files[path]
		except KeyError:
			raise TorrentServiceError('file not found')
		return f['size']
	def request_download(self, infohash, path, pos, size):
		trnt = self._get_torrent(infohash)
		return trnt.request_data(path, pos, size)
	def is_downloaded(self, infohash, path, pos, size):
		trnt = self._get_torrent(infohash)
		return trnt.isdownloaded(path, pos, size)


g_svc = None
g_events = []
def _queue_event(ev):
	global g_events
	if len(g_events) > 300:
		g_events = g_events[-300:]
	g_events.append(ev)
class TorrentFSEventHandler(EventHandler):
	def __init__(self):
		pass
	def torrent_ready(self, trnt):
		pass
	def torrent_removed(self, infohash):
		pass
	def piece_finished(self, trnt, pieceidx):
		_queue_event({'type': 'piece_finished', 'infohash': trnt.infohash, 'piece': pieceidx})


def _test_main(svc):
	#ih1 = svc.add_infohash('dc53e61216ab940047ab88af5155a713da4cbc3b')
	#ih1 = svc.add_file('/home/pjm0616/tmp/tmp.torrent')
	#svc._get_torrent(ih1).handle.connect_peer(('127.0.0.1', 56801), 0)
	#ih2 = svc.add_infohash('0a7a2863392db1d73c1a98a5248e2dfcdd6bfa5b')

	alive = True
	ticks = 0
	while alive:
		svc.tick()
		ticks += 1
		if ticks % 2*4 == 0:
			print('====================')
			out = ''
			for ih in svc.get_torrentlist():
				name = svc.get_torrentname(ih)
				print name

				_trnt = svc._get_torrent(ih)
				s = _trnt.handle.status()
				print 'state: %s; progress: %5.4f%%; total_dn: %db; peers: %d; seeds: %d; distributed copies: %d;' % (
						svc.get_state(ih), s.progress*100, s.total_done, s.num_peers, s.num_seeds, s.distributed_copies
					)

				print 'dn: %s/s(%s); up: %s/s(%s); info-hash: %s; ' % (
						add_suffix(s.download_rate), add_suffix(s.total_download),
						add_suffix(s.upload_rate), add_suffix(s.total_upload),
						ih
					)

				files = svc.get_filelist(ih)
				# calling have_piece without having metadata causes segfault
				if files:
					print 'first file: %s' % files[0]
					print svc.is_downloaded(ih, files[0], 0, 1024 * 1024 * 3)

		time.sleep(0.5)

if __name__ == '__main__':
	evhandler = TorrentFSEventHandler()
	sess = Session(evhandler)
	g_svc = TorrentService(sess)
	g_svc.start()

	try:
		_test_main(g_svc)
	except KeyboardInterrupt:
		pass
	g_svc.stop()





