#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gevent

import gevent.monkey
gevent.monkey.patch_all()
# rpyc imports threading
import rpyc

from torrent import *


g_svc = None
g_events = []
def _queue_event(ev):
	global g_events
	if len(g_events) > 300:
		g_events = g_events[-300:]
	g_events.append(ev)
class TorrentFSEventHandler(EventHandler):
	def __init__(self):
		pass
	def torrent_ready(self, trnt):
		pass
	def torrent_removed(self, infohash):
		pass
	def piece_finished(self, trnt, pieceidx):
		_queue_event({'type': 'piece_finished', 'infohash': trnt.infohash, 'piece': pieceidx})

class TorrentFSRpcService(rpyc.Service):
	def _rpyc_getattr(self, name):
		if name.startswith('_'):
			raise AttributeError('not allowed')
		try:
			r = getattr(self, name)
		except AttributeError:
			return getattr(g_svc, name)
	def register_callback(self, rpcfunc):
		self.callback = rpcfunc
	def pop_event(self):
		global g_events
		ev, g_events = g_events[0], g_events[1:]
		return ev






def main(svc):
	#ih1 = svc.add_infohash('dc53e61216ab940047ab88af5155a713da4cbc3b')
	#ih1 = svc.add_file('/home/pjm0616/tmp/tmp.torrent')
	#svc._get_torrent(ih1).handle.connect_peer(('127.0.0.1', 56801), 0)
	#ih2 = svc.add_infohash('0a7a2863392db1d73c1a98a5248e2dfcdd6bfa5b')

	alive = True
	ticks = 0
	while alive:
		svc.tick()
		ticks += 1
		if ticks % 2*4 == 0:
			print('====================')
			out = ''
			for ih in svc.get_torrentlist():
				name = svc.get_torrentname(ih)
				print name

				_trnt = svc._get_torrent(ih)
				s = _trnt.handle.status()
				print 'state: %s; progress: %5.4f%%; total_dn: %db; peers: %d; seeds: %d; distributed copies: %d;' % (
						svc.get_state(ih), s.progress*100, s.total_done, s.num_peers, s.num_seeds, s.distributed_copies
					)

				print 'dn: %s/s(%s); up: %s/s(%s); info-hash: %s; ' % (
						add_suffix(s.download_rate), add_suffix(s.total_download),
						add_suffix(s.upload_rate), add_suffix(s.total_upload),
						ih
					)

				files = svc.get_filelist(ih)
				# calling have_piece without having metadata causes segfault
				if files:
					print 'first file: %s' % files[0]
					print svc.is_downloaded(ih, files[0], 0, 1024 * 1024 * 3)

		gevent.sleep(0.5)

if __name__ == '__main__':
	evdispatcher = TorrentFSEventHandler()
	sess = Session(evdispatcher)
	g_svc = TorrentService(sess)
	g_svc.start()

	from rpyc.utils.server import ThreadedServer
	t = ThreadedServer(TorrentFSRpcService, port = 18861)
	gevent.Greenlet.spawn(t.start)

	try:
		main(g_svc)
	except KeyboardInterrupt:
		pass
	g_svc.stop()



