#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
from errno import ENOENT, EPERM, EINVAL
import stat
import fcntl
import time
import re

import fuse
import rpyc

fuse.fuse_python_api = (0, 2)

# We use a custom file class
fuse.feature_assert('stateful_files', 'has_init')

_re_btfs_path = re.compile('^/([0-9a-z]{40})(?:/(.*))?$')
def parse_fspath(path):
	'''
		Returns ('', '') if path == '/'
		Returns (infohash, '') if path == '/infohash'
		Returns (infohash, tpath) otherwise
		raises OSError if path is invalid
		`tpath' does not start or end with a '/'
	'''
	res = _re_btfs_path.match(path)
	if not res:
		if path == '/':
			return '', ''
		else:
			raise OSError('open failed: %s' % path)
	tpath = res.group(2) or ''
	if tpath.endswith('/'):
		raise OSError('open failed: %s' % path)
	return res.group(1), tpath

class TorrentSvcConnection(object):
	def __init__(self, host, port):
		self.addr = (host, port)
		self.conn = rpyc.connect(host, port)
	def __getattr__(self, name):
		try:
			return getattr(self.__dict__, name)
		except AttributeError:
			return getattr(self.conn.root, name)

	def wait_for_download(self, infohash, path, offset, length):
		pass
g_svc = None

class TorrentFSEntry(object):
	MODE_FILE = stat.S_IFREG | 0444
	MODE_DIR = stat.S_IFDIR | 0555
	def __init__(self, fspath):
		self.name = ''
		self.info = {'st_mode': 0, 'st_size': 0}
		self.valid = True

		self.files = {} # not empty if dir
		try:
			self.infohash, self.tpath = parse_fspath(fspath)
		except OSError:
			self.valid = False
			print 'btfsobj(%s): invalid path' % fspath
			return

		self._makedir()
		#print 'btfsobj(%s): name: %s; info: %r; files: %s' % (fspath, self.name, self.info, ', '.join(self.files.keys()))

	def _makedir(self):
		if not self.infohash:
			self.info['st_mode'] = self.MODE_DIR
			for infohash in g_svc.get_torrentlist():
				self.files[infohash] = {'st_mode': self.MODE_DIR}
		else:
			trnt_dirtree = g_svc.get_dirtree(self.infohash)
			assert self.tpath == self.tpath.strip('/')
			fs_hierarchy = self.tpath.split('/')

			self.name = fs_hierarchy[-1]
			if self.name == '':
				file = trnt_dirtree
				self.name = self.infohash
			else:
				root = trnt_dirtree
				for entry in fs_hierarchy[:-1]:
					try:
						root = root[entry]
					except KeyError:
						self.valid = False
						return
				try:
					file = root[self.name]
				except KeyError:
					self.valid = False
					return

			if type(file) is int:
				# `file' == file size
				self.info['st_mode'] = self.MODE_FILE
				self.info['st_size'] = file
			else:
				self.info['st_mode'] = self.MODE_DIR
				#for entry, file2 in file.iteritems(): # rpyc does not support dict.iteritems()
				for entry in file:
					file2 = file[entry]
					if type(file2) is int:
						self.files[entry] = {'st_mode': self.MODE_FILE, 'st_size': file2}
					else:
						self.files[entry] = {'st_mode': self.MODE_DIR}

class TorrentFS(fuse.Fuse):
	def __init__(self, *args, **kwargs):
		fuse.Fuse.__init__(self, *args, **kwargs)
		self.file_class = TorrentFSFile

	def readdir(self, path, offset):
		print 'readdir: %s' % path
		fsobj = TorrentFSEntry(path)
		if not fsobj.valid:
			print '\tnot valid'
			return
		print '\tfiles: %s' % ', '.join(fsobj.files)
		for name, info in fsobj.files.iteritems():
			t = info['st_mode'] & stat.S_IFDIR
			yield fuse.Direntry(name, type=t)

	def access(self, path, mode):
		print 'access: %s %o' % (path, mode)
		fsobj = TorrentFSEntry(path)
		if fsobj.valid:
			return 0
		else:
			return -ENOENT
	def utime(self, path, times):
		os.utime("." + path, times)
	def utimens(self, path, ts_acc, ts_mod):
		raise NotImplementedError()

	def getattr(self, path):
		print 'stat: %s' % path
		fsobj = TorrentFSEntry(path)
		if not fsobj.valid:
			print '\tERR: not valid'
			return -ENOENT

		size = fsobj.info['st_size']
		mode = fsobj.info['st_mode']
		try:
			st = os.lstat("." + path)
			mtime = st.st_mtime
		except OSError:
			mtime = 0

		dic = {
			'st_mode': mode, 'st_ino': 0, 'st_dev': 0, 'st_nlink': 1, 'st_uid': 0, 'st_gid': 0,
			'st_size': size, 'st_atime': 0, 'st_mtime': mtime, 'st_ctime': 0}
		st = fuse.Stat()
		st.__dict__.update(dic)
		print '\t%r' % dic
		return st
	def statfs(self):
		# hack
		return os.statvfs(".")

	def fsinit(self):
		os.chdir(self.srvroot)

	def readlink(self, path):
		return -EINVAL
	def unlink(self, path):
		return -EPERM
	def rmdir(self, path):
		return -EPERM
	def symlink(self, path, path1):
		return -EPERM
	def rename(self, path, path1):
		return -EPERM
	def link(self, path, path1):
		return -EPERM
	def chmod(self, path, mode):
		return -EPERM
	def chown(self, path, user, group):
		return -EPERM
	def truncate(self, path, len):
		return -EPERM
	def mknod(self, path, mode, dev):
		return -EPERM
	def mkdir(self, path, mode):
		return -EPERM

class TorrentFSFileObj(object):
	def __init__(self, infohash, tpath):
		self.infohash = infohash
		self.tpath = tpath
		self.fspath = './%s/%s' % (self.infohash, self.tpath)
	def _openfile(self):
		if self.fd >= 0:
			return
		try:
			self.fd = os.open(self.fspath, os.O_RDONLY, *mode)
		except OSError as e:
			self.fd = -1
			return
		self.file = os.fdopen(self.fd, os.O_RDONLY)

	def read(self, length, offset):	
		if not g_svc.is_downloaded(self.infohash, self.fname, offset, length):
			g_svc.wait_for_download(self.infohash, self.fname, offset, length)

class TorrentFSFile(object):

	def __init__(self, path, flags, *mode):
		print 'open: %s %d' % (path, flags)
		fsobj = TorrentFSEntry(path)
		if not fsobj.valid:
			raise OSError(ENOENT)
		self.infohash, self.tpath = fsobj.infohash, fsobj.tpath
		print '\tih=%s; tpath=%s' % (self.infohash, self.tpath)
		if not self.infohash or not self.tpath:
			raise OSError(EPERM)

		self.file = TorrentFSFileObj(self.infohash, self.tpath)

	def read(self, length, offset):
		self.file.seek(offset)
		return self.file.read(length)

	def release(self, flags):
		self.file.close()

	def fgetattr(self):
		return os.fstat(self.fd)

	def _fflush(self):
		return 0
	def fsync(self, isfsyncfile):
		return 0
	def flush(self):
		return 0
	def write(self, buf, offset):
		return -EPERM
	def ftruncate(self, len):
		return -EPERM
	def lock(self, cmd, owner, **kw):
		return -EPERM



def main():
	server = TorrentFS(version="%prog " + fuse.__version__, usage=fuse.Fuse.fusage)
	server.multithreaded = True
	server.parser.add_option(mountopt="srvroot", metavar="PATH", default='./', help="server root directory(contains configs) [default: %default]")
	server.parse(values=server, errex=1)
	print server.srvroot

	try:
		if server.fuse_args.mount_expected():
			os.chdir(server.srvroot)
	except OSError:
		print 'chdir failed'
		sys.exit(1)

	server.main()

if __name__ == '__main__':
	g_svc = TorrentSvcConnection('localhost', 18861)
	main()


