import rpyc

ih = 'dc53e61216ab940047ab88af5155a713da4cbc3b'

class TorrentSvcConnection(object):
	def __init__(self, host, port):
		self.addr = (host, port)
		self.conn = rpyc.connect(host, port)
	def __getattr__(self, name):
		try:
			return getattr(self.__dict__, name)
		except AttributeError:
			return getattr(self.conn.root, name)


svc = TorrentSvcConnection('localhost', 18861)
print svc.add_infohash(ih)
print svc.get_torrentlist()
#print svc.remove_torrent(ih)

#print svc.is_downloaded(ih, svc.get_filelist(ih)[0], 1024*1024*10, 1024*1024*1)
#print svc.request_download(ih, svc.get_filelist(ih)[0], 1024*1024*10, 1024*1024*1)

